﻿using CommunicationServer.IoCommunication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading.Tasks;

namespace CommunicationServer
{
    class Program
    {
        public static IHostBuilder CreateHostBuilder() =>
            Host.CreateDefaultBuilder()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<TcpServer>();
                });

        private static async Task Main(string[] args)
        {
            using IHost host = CreateHostBuilder().Build();

            await host.RunAsync();
        }
    }
}
