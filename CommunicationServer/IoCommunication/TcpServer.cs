﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;

namespace CommunicationServer.IoCommunication
{
    public static class StreamExtension
    {
        public static void WriteLineFlush(this StreamWriter s, string message)
        {
            s.WriteLine(message);
            s.Flush();
        }
    }

    public class TcpServer : BackgroundService
    {
        private string Name => nameof(TcpServer);
        private readonly ILogger<TcpServer> _logger;
        private readonly string IP;
        private readonly int GmPort;
        private readonly int AgentPort;
        private System.Net.Sockets.TcpListener _gmServer;
        private System.Net.Sockets.TcpListener _agentServer;

        private StreamWriter GMWriter;
        private readonly ConcurrentDictionary<int, StreamWriter> agents = new ConcurrentDictionary<int, StreamWriter>();

        private JsonSerializer serializer = new JsonSerializer
        {
            NullValueHandling = NullValueHandling.Include,
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
        };

        private readonly List<Stream> streams = new List<Stream>();
        private readonly List<System.Net.Sockets.TcpClient> clients
            = new List<System.Net.Sockets.TcpClient>();

        private readonly IEnumerator<int> Ids = Enumerable.Range(0, 1000).OrderBy(p => Guid.NewGuid()).GetEnumerator();

        public int nextId
        {
            get
            {
                if (Ids.MoveNext())
                    return Ids.Current;
                else
                    throw new IndexOutOfRangeException();
            }
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            return base.StartAsync(cancellationToken);
        }

        public override void Dispose()
        {
            _logger.LogInformation(
                $"Shutting down {Name}!");
            _gmServer.Stop();
            _agentServer.Stop();

            base.Dispose();
        }
        public TcpServer(
            ILogger<TcpServer> logger,
            IConfiguration configuration)
        {
            _logger = logger;
            IP = configuration.GetValue<string>("CsIP", "127.0.0.1");
            GmPort = configuration.GetValue<int>("GmPort", 32323);
            AgentPort = configuration.GetValue<int>("AgentPort", 32322);
        }

        private string Banner()
        {
            var mainInfo = $"{Name} connected CM= {IP}:{GmPort} AGENT= {IP}:{AgentPort}\n";
            var header = new String('=', mainInfo.Length - 1) + "\n";
            header += header;
            return "\n" + header + mainInfo + header;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            try
            {
                _gmServer = new System.Net.Sockets.TcpListener(IPAddress.Parse(IP), GmPort);
                _agentServer = new System.Net.Sockets.TcpListener(IPAddress.Parse(IP), AgentPort);
                _gmServer.Start();
                _agentServer.Start();
                cancellationToken.Register(() => { _gmServer.Stop(); _agentServer.Stop(); });

                _logger.LogInformation(
                    Banner());

                try
                {
                    _logger.LogInformation($"Waiting for GM");

                    using var client = await _gmServer.AcceptTcpClientAsync();
                    using var stream = client.GetStream();

                    _logger.LogInformation($"GM connected");

                    var GMReader = new StreamReader(stream, Encoding.UTF8);
                    GMWriter = new StreamWriter(stream, new UTF8Encoding(false));

                    var t = Task.Run(() => MainGMLoop(GMReader), cancellationToken);
                    var t2 = Task.Run(() => MainAgentAcceptationLoop(cancellationToken), cancellationToken);

                    Task.WaitAll(t, t2);
                }
                catch (ObjectDisposedException) { }

            }
            catch (Exception e)
            {
                _logger.LogError(e, "");
            }
        }

        private void MainAgentAcceptationLoop(CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Waiting for Agets");

            while (!cancellationToken.IsCancellationRequested)
            {
                var AgentClient = _agentServer.AcceptTcpClient();
                var AgentStream = AgentClient.GetStream();

                int id = nextId;
                _logger.LogInformation($"Agent {id} connected");

                clients.Add(AgentClient);
                streams.Add(AgentStream);

                var AgentReader = new StreamReader(AgentStream, Encoding.UTF8);
                var AgentWriter = new StreamWriter(AgentStream, new UTF8Encoding(false));

                agents.TryAdd(id, AgentWriter);


                _ = Task.Factory.StartNew(() => MainAgentLoop(id, AgentReader), cancellationToken);
            }
        }

        private void MainGMLoop(StreamReader reader)
        {
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                _logger.LogInformation($"Received {line} from CS");

                // TODO check agents id from json
                int? id = serializer.Deserialize<MessageWrapperDTO>(
                    new JsonTextReader(new StringReader(line))).AgentId;

                if (!id.HasValue)
                {
                    foreach (var agent in agents.Values)
                    {
                        lock (agent)
                        {
                            agent.WriteLineFlush(line);
                        }
                    }
                }
                else
                {
                    lock (agents[id.Value])
                    {
                        agents[id.Value].WriteLineFlush(line);
                    }
                }
            }
        }

        private void MainAgentLoop(int id, StreamReader reader)
        {
            _logger.LogInformation($"Started Agent:{id} main loop");

            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                _logger.LogInformation($"Received {line} from Agent {id}");

                var wrapper = serializer.Deserialize<MessageWrapperDTO>(
                    new JsonTextReader(new StringReader(line)));

                var writer = new StringWriter();
                serializer.Serialize(writer, new MessageWrapperDTO
                {
                    MessageId = wrapper.MessageId,
                    AgentId = id,
                    Payload = wrapper.Payload,
                });
                lock (GMWriter)
                {
                    GMWriter.WriteLineFlush(writer.ToString());
                }
            }
        }
    }
}
