﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommunicationServer.IoCommunication
{
    public class MessageWrapperDTO
    {
        public int MessageId { get; set; }
        public int? AgentId { get; set; }
        public object Payload { get; set; }
    }
}
